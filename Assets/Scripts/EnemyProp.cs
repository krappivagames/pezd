﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyProp : MonoBehaviour
{
    Transform[] targetArr = WagonsProp.wagonArr;
    Transform target;
    public float speed = .3f;
    public int hp = 5;
    public bool isRight = false;

    private void Start()
    {
        int arrNum = Random.Range(0, targetArr.Length);
        target = targetArr[arrNum];
        for (int i = 0; i < targetArr.Length; i++)
        {
            if (!target.gameObject.activeSelf && arrNum >= 0)
            {
                target = targetArr[--arrNum];
            }
        }
    }

    private void FixedUpdate()
    {
        if (gameObject.activeSelf)
        {
            transform.Translate((target.position - transform.position).normalized * speed * Time.fixedDeltaTime);
        }
    }

    private void OnParticleCollision(GameObject other)
    {
        hp--;

        if (hp <= 0)
        {
            Destroy(gameObject);
        }
    }
}
