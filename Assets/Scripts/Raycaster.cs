﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Raycaster : MonoBehaviour
{
    Vector2 pos;
    RaycastHit hit;

    void Start()
    {

    }

    void Update()
    {
        //TouchRay();
        MouseClickRay();
    }

    private void MouseClickRay()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RotateCannon(ray);
        }
    }

    private void RotateCannon(Ray ray)
    {
        if (Physics.Raycast(ray, out hit))
        {
            if (hit.transform.GetComponentInChildren<CannonProp>())
            {
                hit.transform.GetComponentInChildren<CannonProp>().OnRaycastHit();
            }
        }
    }

    private void TouchRay()
    {
        if (Input.touchCount > 0)
        {
            if (Input.GetTouch(0).phase == TouchPhase.Began)
            {
                Ray ray = Camera.main.ScreenPointToRay(Input.GetTouch(0).position);
                RotateCannon(ray);
            }
        }
    }
}
