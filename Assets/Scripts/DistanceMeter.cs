﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DistanceMeter : MonoBehaviour
{
    public Text distanceText;
    public float speed = 5f;

    private float distance = 0f;

    void Update()
    {
        distance += Time.deltaTime * speed;

        distanceText.text = distance.ToString("0") + " m";
    }
}
