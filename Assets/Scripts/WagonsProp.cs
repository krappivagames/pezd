﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WagonsProp : MonoBehaviour
{
    public static Transform[] wagonArr = new Transform[3]; // if wagons number will be more than 3, this value need to be changed

    private void Start()
    {
        for (int i = 0; i < transform.childCount; i++)
        {
            wagonArr[i] = transform.GetChild(i);            
        }
    }
}
