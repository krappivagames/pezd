﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CannonProp : MonoBehaviour
{
    Transform target;
    ParticleSystem bullets;
    public bool CannonRightPosition = true;

    private void Start()
    {
        bullets = GetComponentInChildren<ParticleSystem>();
    }

    private void Update()
    {
        SetTargetEnemy();
        Aim();

    }

    private void Shoot(bool isActive)
    {
        var emission = bullets.emission;
        emission.enabled = isActive;
    }

    private void Aim()
    {
        if (target == null)
        {
            Shoot(false);
            return;
        }

        if (Vector3.Distance(transform.position, target.position) < 9f)
        {
            transform.LookAt(target, new Vector3(0, 0, -1));
            Shoot(true);
        }
        else
        {
            Shoot(false);
        }
    }

    private void SetTargetEnemy()
    {
        var sceneEnemies = FindObjectsOfType<EnemyProp>();
        if (sceneEnemies.Length == 0) { return; }

        foreach (EnemyProp enemy in sceneEnemies)
        {

            if (enemy.isRight == CannonRightPosition)
            {
                if (target == null)
                {
                    target = enemy.transform;
                    return;
                }

                target = GetClosest(target, enemy.transform);
            }
        }
    }

    private Transform GetClosest(Transform targetEnemy, Transform checkedEnemy)
    {
        var distToTarget = Vector3.Distance(transform.position, targetEnemy.position);
        var distToEnemy = Vector3.Distance(transform.position, checkedEnemy.transform.position);

        if (distToEnemy < distToTarget)
        {
            targetEnemy = checkedEnemy;
        }

        return targetEnemy;
    }

    public void OnRaycastHit()
    {
        CannonRightPosition = !CannonRightPosition;
        target = null;
        if (CannonRightPosition)
        {
            transform.eulerAngles = new Vector3(0f, 90f, -90f);
        }
        else
        {
            transform.eulerAngles = new Vector3(0f, -90f, -90f);
        }

    }
}
