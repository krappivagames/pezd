﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawn : MonoBehaviour
{
    public GameObject rightSpawner;
    public GameObject leftSpawner;
    public GameObject enemyPrefab;
    public GameObject enemiesParent;

    public int maxEnemies = 10;

    private bool isLeft = true;

    void FixedUpdate()
    {
        if (enemiesParent.transform.childCount < maxEnemies)
        {
            if (isLeft)
            {
                Instantiate(enemyPrefab, new Vector3(leftSpawner.transform.position.x, Random.Range(-16f, 16f)), Quaternion.identity, enemiesParent.transform);
            }
            else
            {
                var enemy = Instantiate(enemyPrefab, new Vector3(rightSpawner.transform.position.x, Random.Range(-16f, 16f)), Quaternion.identity, enemiesParent.transform);
                enemy.GetComponent<EnemyProp>().isRight = true;
            }
            isLeft = !isLeft;
        }
    }
}
